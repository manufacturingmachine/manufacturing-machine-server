import {Client, ConnectConfig} from "ssh2";
import {DatabaseConfig} from "./databse-structure";
import {Connection} from "promise-mysql";

const mysql = require('mysql2/promise');

/**
 * @const ssh2.Connection _conn The SSH connection
 */
let sshConnection: Client = null;
/**
 * @const mysql2.Connection _conn The MySQL connection
 */
let sqlConnection: Connection = null;

/**
 * @param sshConfig SSH Configuration as defined by ssh2 package
 * @param dbConfig MySQL Configuration as defined by mysql(2) package
 * @return Promise <mysql2 connection>
 */
export function mySqlSSHConnect(sshConfig: ConnectConfig, dbConfig: DatabaseConfig): Promise<Connection> {
    dbConfig = addDefaults(dbConfig);
    return new Promise(function (resolve, reject) {
        sshConnection = new Client();
        sshConnection.on('ready', function () {
            sshConnection.forwardOut(
                '127.0.0.1',
                12345,
                dbConfig.host,
                dbConfig.port,
                function (err, stream) {
                    if (err) {
                        mySqlSSHClose();
                        return reject((<any>err).reason == 'CONNECT_FAILED' ? 'Connection failed.' : err)
                    }

                    // override db host, since we're operating from within the SSH tunnel
                    dbConfig.host = 'localhost';
                    dbConfig.stream = stream;

                    sqlConnection = mysql.createConnection(dbConfig).then((conn: Connection) => {
                        sqlConnection = conn;
                        resolve(sqlConnection)
                    });
                }
            )
        }).connect(sshConfig)
    })
}

export async function mySqlSSHClose() {
    if ('end' in sqlConnection) {
        await sqlConnection.end();
    }

    if ('end' in sshConnection) {
        sshConnection.end()
    }
}

function addDefaults(dbConfig: DatabaseConfig) {
    if (dbConfig.port == null) {
        dbConfig.port = 3306
    }

    if (dbConfig.host == null) {
        dbConfig.host = 'localhost'
    }

    return dbConfig
}
