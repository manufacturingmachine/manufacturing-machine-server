export const enum MqttTopic {
    XDK110 = 'BCDS/XDK110/sensors/data',
    ManufacturingMachine = 'ManufacturingMachine'
}
