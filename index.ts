import {connectToMsSqlServer, sqlConnection} from "./app/database/mysql-connect";
import {Database} from "./app/database/databse-structure";
import {generateData} from "./app/manufacturing-machine-real-time-simulator";
import {KoaServer} from "./app";
import * as mqtt from 'mqtt';
import {MqttTopic} from "./app/utils/mqtt-topic.enum";

let client: mqtt.MqttClient;
console.log('Connecting to MQTT broker');
client = mqtt.connect('ws://mqtt-broker-server:3000');
console.log('Connected to MQTT broker');

client.on('connect', function () {
    console.log('Connected to MQTT broker');
    client.subscribe(MqttTopic.ManufacturingMachine, (err) => {
        console.log('Subscribed to', MqttTopic.ManufacturingMachine);
        if (!err) {
            setup().catch(err => console.error(err));
        } else {
            console.log(err);
        }
    })
});

client.on('error', function (error) {
    console.error(error)
});

client.on('reconnect', function () {
    console.log('Reconnecting')
});

// client.on('message', function (topic, message) {
//     // message is Buffer
//     console.log(message.toString());
//     client.end()
// });


// fired when the mqtt server is ready
async function setup() {
    await connectToMsSqlServer(
        {
            database: Database.MyDb,
            host: '127.0.0.1',
            port: 3306,
            user: 'root',
            password: 'cloudera',
            connectionLimit: 10,
        },
        {
            host: '10.111.0.250',
            port: 22,
            username: 'team6',
            password: 'team6_b1i2'
        }
    );

    const mySqlDbConnection = sqlConnection.get(Database.MyDb);
    const [results, fields] = await mySqlDbConnection.client.query('SELECT MAX(part_ID) as currentPartId FROM Components');

    console.log('START Manufacturing machine simulator');
    const SIMULATOR = 'Manufacturing machine simulator';
    console.time(SIMULATOR);
    generateData(client, results.length > 0 ? Number.parseInt(results[0].currentPartId) : null)
        .then(() => {
            console.log('Manufacturing machine simulator finish to generate data');
            console.timeEnd(SIMULATOR);
        })
        .catch(err => console.error(err));

    await (new KoaServer()).start();
}
